package com.zero9f9.pushr;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;


public class ClientActivity extends Activity {

    private Socket socket;

    private static final int SERVERPORT = 5000;
    private static final String SERVER_IP = "192.168.1.191";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);

        final EditText et = (EditText) findViewById(R.id.EditText01);
        et.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.d("Keyevent: ", KeyEvent.keyCodeToString(keyCode));
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    sendResponse(et);
                    return true;
                }
                return false;
            }
        });


        new Thread(new ClientThread()).start();
    }

    public void onClick(View view) {
        final EditText et = (EditText) findViewById(R.id.EditText01);
        sendResponse(et);
    }

    public boolean sendResponse(EditText et) {
        String str = et.getText().toString();
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(
                    new OutputStreamWriter(socket.getOutputStream())),
                    true);
            out.println(str);
//            updateListing(str);
            et.setText(""); // clear out text once set

        } catch (IOException e) {
        }
        return false;
    }

    public void updateListing(String string) {
        TextView textView = (TextView) findViewById(R.id.textView);
        if (textView.getText().length() > 0) {
            textView.setText(textView.getText() + "\n" + string);
        }
    }

    public void setSwitch(boolean state) {
        Switch sw = (Switch) findViewById(R.id.switch2);
        sw.setChecked(true);
    }

    class ClientThread implements Runnable {

        @Override
        public void run() {
            try {
                InetAddress serverAddr = InetAddress.getByName(SERVER_IP);

                socket = new Socket(serverAddr, SERVERPORT);
                new Thread(new ReceiverThread()).start();
                if (socket.isConnected()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setSwitch(true);
                        }
                    });
                }

            } catch (UnknownHostException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
    }
    class ReceiverThread implements Runnable {
        private byte[] buffer;

        public ReceiverThread() {
            buffer = new byte[128];
        }
        @Override
        public void run() {
            try {
                if (socket == null)
                    Log.d("Receiverthread:", "socket is null");
                if (socket != null && socket.isConnected()) {
                    InputStream inputStream = socket.getInputStream();
                    int read = inputStream.read(buffer);
                    while (read != -1) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateListing(new String(buffer));
                            }
                        });
                        read = inputStream.read(buffer);
                    }
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}